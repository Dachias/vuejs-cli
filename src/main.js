import Vue from 'vue'
import App from './App.vue'
//global nesting of components
//import Ninjas from './Ninja.vue'
//
//Vue.component('ninjas',Ninjas);

new Vue({
  el: '#app',
  render: h => h(App)
})
